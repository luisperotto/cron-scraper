require("dotenv").config({ path: ".env" });

const DB = process.env.DB;
const BOT_TOKEN = process.env.BOT_TOKEN;
const CHAT_ID = process.env.CHAT_ID;

module.exports = {
	DB,
	BOT_TOKEN,
	CHAT_ID,
};
