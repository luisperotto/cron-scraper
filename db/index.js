const { DB } = require("../config");

const knex = require("knex")({
	client: "mysql2",
	connection: DB,
});

module.exports = knex;
