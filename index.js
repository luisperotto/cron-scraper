const http = require("http");
const server = http.createServer();
const db = require("./db");
const cron = require("node-cron");
const moment = require("moment");
const pm2 = require("pm2");
const TelegramBot = require("node-telegram-bot-api");
const {
  BOT_TOKEN,
  CHAT_ID,
} = require("./config");

const bot = new TelegramBot(BOT_TOKEN, {
  polling: true,
});
const { exec } = require("child_process");

async function deleteAssets() {
  try {
    exec(
      "cd ../bo-bank-scraper && ls",
      (error, stdout, stderr) => {
        if (error) {
          console.error(error);
        } else {
          if (stdout.includes("asset")) {
            exec(
              "cd ../bo-bank-scraper/asset && rm -rf *",
              (error, stdout, stderr) => {
                if (error) {
                  console.error(error);
                } else {
                  console.log(
                    "DELETE:::",
                    stderr
                  );
                }
              }
            );
          }
        }
      }
    );
  } catch (error) {
    console.error(error);
  }
}

async function setAvailable() {
  try {
    let accounts = await db("bank_accounts")
      .where("is_deleted", 0)
      .select(
        "id",
        "scraper_status",
        "is_available"
      );

    accounts = await Promise.all(
      accounts.map(async (el) => {
        if (el.scraper_status === "running") {
          console.log(el, "running");
          await db("bank_accounts")
            .where("id", el.id)
            .update({
              is_available: 1,
            });
        } else {
          console.log(el, "stopped");
          await db("bank_accounts")
            .where("id", el.id)
            .update({
              is_available: 0,
            });
        }
      })
    );
  } catch (error) {
    console.error(error);
  }
}

async function forceRandomAvailableForScraper() {
  try {
    await fetch(
      "https://api-pay.s88pay.net/api/apk/forceRandomAvailableForScraper"
    );
  } catch (error) {
    console.error(error);
  }
}

async function maintenanceMehsana() {
  let bank_account = await db("bank_accounts")
    .where({
      bank_id: 4,
    })
    .select("account_numbers as ac");
  let ac = bank_account.map((el) => el.ac);
  let data = await Promise.all(
    ac.map(async (el) => {
      return await stopPM2(el);
    })
  );

  let up = await db("bank_accounts")
    .whereIn("account_numbers", ac)
    .update({
      is_available: false,
      scraper_status: "stopped",
    });
  console.log(data, up);

  await bot.sendMessage(
    CHAT_ID,
    `Mehsana Bank channel are already closed… \nStopping bot …. Please wait some minutes…`
  );
  await new Promise((r) =>
    setTimeout(r, 60 * 1000)
  );
  await bot.sendMessage(
    CHAT_ID,
    `Mehsana Bank are already stopped`
  );
}

async function resetAllBankAccount() {
  try {
    let bank_account = await db(
      "bank_accounts"
    ).select("account_numbers as ac");

    let ac = bank_account.map((el) => el.ac);

    let data = await Promise.all(
      ac.map(async (el) => {
        return await stopPM2(el);
      })
    );

    let up = await db("bank_accounts")
      .whereIn("account_numbers", ac)
      .update({
        is_available: false,
        scraper_status: "stopped",
      });
    console.log(data, up);

    await bot.sendMessage(
      CHAT_ID,
      `All bank accounts channel are already closed… \nStopping bot …. Please wait some minutes…`
    );
    await new Promise((r) =>
      setTimeout(r, 300 * 1000)
    );
    await bot.sendMessage(
      CHAT_ID,
      `All bots are already stopped`
    );
  } catch (error) {
    console.error(error);
  }
}

async function stopPM2(ac) {
  if (ac) {
    pm2.connect((error) => {
      if (error) return error.message;

      pm2.stop(ac, (err, proc) => {
        if (err) return err.message;
        pm2.disconnect();
      });
    });
    return true;
  } else {
    return "no bank";
  }
}

async function cleanProxyIndusindPersonal() {
  let count_base = await db("scraper_proxies")
    .whereNot({
      server: "local",
    })
    .count("id as id")
    .first();
  let bank_data = await db("bank_accounts")
    .where({
      bank_id: 18,
    })
    .select("id");

  let data = await Promise.all(
    bank_data.map(async (el) => {
      let count_history = await db(
        "scraper_proxies_bank_accounts"
      )
        .where({
          bank_account_id: el.id,
        })
        .count("id as id")
        .first();
      if (
        count_history.id >
        count_base.id - bank_data.length
      ) {
        return {
          account: el.id,
          status: true,
          count_history: count_history.id,
        };
      } else {
        return {
          account: el.id,
          status: false,
          count_history: count_history.id,
        };
      }
    })
  );

  console.log(
    "count base indusind",
    count_base,
    data
  );

  let reached_limit = data
    .filter((el) => el.status === true)
    .map((el) => el.account);

  let deleted = await db(
    "scraper_proxies_bank_accounts"
  )
    .whereIn("bank_account_id", reached_limit)
    .del();
  console.log("LIMIT:::::", deleted);
}

async function cleanProxy() {
  let count_base = await db("scraper_proxies")
    .whereNot({
      server: "local",
    })
    .count("id as id")
    .first();
  let bank_data = await db("bank_accounts")
    .where({
      bank_id: 2,
    })
    .select("id");

  let data = await Promise.all(
    bank_data.map(async (el) => {
      let count_history = await db(
        "scraper_proxies_bank_accounts"
      )
        .where({
          bank_account_id: el.id,
        })
        .count("id as id")
        .first();
      if (
        count_history.id >
        count_base.id - bank_data.length
      ) {
        return {
          account: el.id,
          status: true,
          count_history: count_history.id,
        };
      } else {
        return {
          account: el.id,
          status: false,
          count_history: count_history.id,
        };
      }
    })
  );

  console.log("count base", count_base, data);

  let reached_limit = data
    .filter((el) => el.status === true)
    .map((el) => el.account);

  let deleted = await db(
    "scraper_proxies_bank_accounts"
  )
    .whereIn("bank_account_id", reached_limit)
    .del();
  console.log("LIMIT:::::", deleted);
}

async function Schedules() {
  let proxy = await db(
    "scraper_proxies_bank_accounts"
  )
    .where({
      is_blocked: true,
    })
    .del();
  console.log(proxy);
}
async function ChatSchedule() {
  let data = await db("bank_account_messages")
    .where(
      "created_at",
      "<",
      moment().format("YYYY-MM-DD hh:mm:ss")
    )
    .del();
  console.log(data);
}

async function checkTransactions() {
  process.env.TZ = undefined;
  let bank_account = await db("bank_accounts")
    .where("scraper_status", "running")
    .where("is_deleted", 0)
    .where("is_available", 1)
    .select("*");

  let less_one = await Promise.all(
    bank_account.map(async (el) => {
      let bank_transactions = await db(
        "bank_transactions"
      )
        .where("bank_account_id", el.id)
        .orderBy("created_at", "desc")
        .first("*");
      let now = moment()
        .utc()
        .format("YYYY-MM-DD HH:mm:ss");
      if (bank_transactions) {
        return {
          bank_id: el.bank_id,
          account_numbers: el.account_numbers,
          holder_name: el.account_holder_name,
          telegram_id: el.telegram_chat_id,
          different_hour: moment(now).diff(
            moment(bank_transactions.created_at),
            "hours"
          ),
          bank_transactions,
        };
      }
    })
  );

  less_one = less_one.filter(
    (el) =>
      el.telegram_id && el.different_hour > 0
  );

  less_one = await Promise.all(
    less_one.map(async (el) => {
      let bank_name = await db("banks")
        .where("id", el.bank_id)
        .first();

      let text = `[ ${moment().format(
        "YYYY-MM-DD HH:mm:ss"
      )} ][newline][newline]${
        bank_name?.name
      } - ${
        el.holder_name
      } Not Comming deposit on this Bank Account Exceed 1 Hours[newline][newline]${
        bank_name.name
      } - ${el.account_numbers} - ${
        el.holder_name
      }`;
      let telegram = await fetch(
        `http://127.0.0.1:3999/telegram/send?account_number=${el.account_numbers}&msg=${text}&with_photo=false`,
        {
          headers: {
            Authorization:
              "Bearer $2y$10$/8kfmsUd2dmEp32VgZp5guenBpwhQNC7R1VElQIa3voIt7VOyXQQO",
          },
        }
      );
      return {
        bank: `${el.holder_name} - ${el.account_holder_name}`,
        data: el,
        status: telegram,
      };
    })
  );
  console.log("check transactions", less_one);
}

// // ======== every 23:45 india / 18:15 UTC ==============
// cron.schedule("15 18 * * *", async () => {
//   await resetAllBankAccount();
// });

// ======== every 24:00 india / 18:30 UTC ==============
cron.schedule("30 18 * * *", async () => {
  await Schedules();
  await ChatSchedule();
});

// ======== every minutes ==============
cron.schedule("* * * * *", async () => {
  cleanProxy();
  cleanProxyIndusindPersonal();
  forceRandomAvailableForScraper();
  deleteAssets();
});

// ========= every 10 minutes =============
cron.schedule("0 */10 * * * *", async () => {
  checkTransactions();
});

// ========= every 00:59 utc set maintenance mehsana
cron.schedule("59 00 * * *", async () => {
  maintenanceMehsana();
});

// ================== NOT USE ===================

// ======== every 20 second ==============
// cron.schedule("*/20 * * * * *", async () => {
//   await setAvailable();
// });

server.listen(7999, () => {
  console.log("Running Server Cron Job");
});
